var LocalStrategy = require('passport-local').Strategy;

var bcrypt = require('bcrypt-nodejs');
var db = require('./database');

module.exports = function(passport) {
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
		db.one('select * from users where id = $1', id).then(function (user) {
	    	done(null, user);
	    });
	});

	passport.use('local-signup', new LocalStrategy({
		usernameField: 'visa',
		passwordField: 'password',
		passReqToCallback: true
	}, function(req, visa, password, done) {

		db.one('select * from users where visa = $1', visa)
		    .then(function (user) {
		    	if (user) {
					return done(null, false, req.flash('signupMessage', 'That visa is already taken.'));
				}
		    }).catch(function (err) {
					db.none('insert into users(visa, password)' +
					    'values($1, $2)', [visa, bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)])
					  .then(function () {
					    db.one('select * from users where visa = $1', visa)
					    .then(function (newUser) {
					    	return done(null, newUser);
					    })
					  })
					  .catch(function (err) {
					    console.log(err);
					  });
		    });
	}));

	passport.use('local-login', new LocalStrategy({
		usernameField: 'visa',
		passwordField: 'password',
		passReqToCallback: true
	}, function(req, visa, password, done) {
		db.one('select * from users where visa = $1', visa)
		    .then(function (user) {
		    	if (!user) {
		    		return done(null, user, req.flash('loginMessage', 'No user found.'));
		    	} else if (!bcrypt.compareSync(password, user.password)) {
					return done(null, user, req.flash('loginMessage', 'Wrong password.'));
				} else {
					return done(null, user);
				}
		    })
		    .catch(function (err) {
		      return done(err);
		    });
	}));
};