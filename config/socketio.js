var db = require('./database');

module.exports = function(io) {
	io.on('connection', function (socket) {
		var addedUser = false;

		// when the client emits 'new message', this listens and executes
		socket.on('new message', function (data, userid, eventid) {
			// we tell the client to execute 'new message'
			db.none('insert into comments (user_id, event_id, content) values($1, $2, $3)', [userid, eventid, data]).then(function () {
			});
			socket.broadcast.emit('new message', {
				username: socket.username,
				message: data
			});
		});

		// when the client emits 'add user', this listens and executes
		socket.on('add user', function (username) {
			if (addedUser) return;
			// we store the username in the socket session for this client
			socket.username = username;
			addedUser = true;
		});

		// when the client emits 'typing', we broadcast it to others
		socket.on('typing', function () {
			socket.broadcast.emit('typing', {
				username: socket.username
			});
		});

		// when the client emits 'stop typing', we broadcast it to others
		socket.on('stop typing', function () {
			socket.broadcast.emit('stop typing', {
				username: socket.username
			});
		});
	});
};