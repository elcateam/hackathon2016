var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Event = require('../app/models/event');
var Comment = require('../app/models/comment');

router.get('/', function(req, res) {
  Event.find().populate('_user', 'visa').exec(function(err, events) {
    console.log(events);
    res.render('events', { title: 'List events', events: events});
  });
});

router.get('/event/:id', function(req, res) {
  var query = {"_id": req.params.id};
  Event.findOne(query).populate('_user', 'visa').exec(function(err, event) {
    console.log(event);
    Comment.find({_event: req.params.id}).populate('_user', 'visa').exec(function(err, comments) {
      console.log(comments);
      // res.render('event', {title : 'Event name: ' + event.name, event : event, comments: comments, userid: req.user.id });
      res.json(event);
    });
  });
});

router.post('/', function(req, res) {
  new Event({name : req.body.name, _user: req.user.id})
  .save(function(err, event) {
    console.log(event);
    res.redirect('/events');
  });
});

router.put('/event/:id', function(req, res) {
  var query = {"_id": req.params.id};
  var update = {name : req.body.name, address : req.body.address, content: req.body.content};
  var options = {new: true};
  Event.findOneAndUpdate(query, update, options, function(err, event){
    console.log(event);
    Comment.find({_event: req.params.id}).populate('_user', 'visa').exec(function(err, comments) {
      console.log(comments);
      res.render('event', {title : 'Event name: ' + event.name, event : event, comments: comments, userid: req.user.id });
    });
  });
});

router.delete('/event/:id', function(req, res) {
  var query = {"_id": req.params.id};
  Event.findOneAndRemove(query, function(err, event){
    console.log(event)
    res.redirect('/events');
  });
});

router.post('/event/:id', function(req, res) {
  new Comment({ _user: req.user.id, _event: req.params.id, content: req.body.commentContent })
  .save(function(err, comment) {
    console.log(comment);
  });
});

router.get('/dummyData', function(req, res) {
  for (var i = 0; i <= 10; i++) {
    new Event({name : 'Event' + i, _user: req.user.id}).save(function(err, event) {
      console.log(event);
      for (var j = 0; j <= 10; j++) {
        new Comment({ _user: req.user.id, _event: event.id, content: 'Comment' + j })
        .save(function(err, comment) {
          console.log(comment);
        });
      }
    });
  };
  res.redirect('/events');
});

module.exports = router;