use events;
db.users.insertMany(
	[
		{ visa: 'SVN', password: 'vinh', email: 'svn@elca.vn'},
		{ visa: 'HPT', password: 'phuong', email: 'hpt@elca.vn'},
	]
);

// Login the Events app by one of the created user above
// Go: http://localhost:8080/events/dummyData for insert dummy Event data.

// Export all collection in mongoDB
// 1. Run the mongodb service first.
// 2. mongodump -d <database name> -o <directory_backup>
// And to 'restore/import', (from directory backkup above)
// 1. mongostore -d <database name>