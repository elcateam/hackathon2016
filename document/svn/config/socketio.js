var Comment = require('../app/models/comment');
var mongoose = require('mongoose');

module.exports = function(io) {
	io.on('connection', function (socket) {
		var addedUser = false;

		// when the client emits 'new message', this listens and executes
		socket.on('new message', function (data, userid, eventid) {
			// we tell the client to execute 'new message'
			new Comment({ _user: userid, _event: eventid, content: data, created_at: new Date() })
				.save(function(err, comment) {
					console.log(comment);
				});
			socket.broadcast.emit('new message', {
				username: socket.username,
				message: data
			});
		});

		// when the client emits 'add user', this listens and executes
		socket.on('add user', function (username) {
			if (addedUser) return;
			// we store the username in the socket session for this client
			socket.username = username;
			addedUser = true;
		});

		// when the client emits 'typing', we broadcast it to others
		socket.on('typing', function () {
			socket.broadcast.emit('typing', {
				username: socket.username
			});
		});

		// when the client emits 'stop typing', we broadcast it to others
		socket.on('stop typing', function () {
			socket.broadcast.emit('stop typing', {
				username: socket.username
			});
		});
	});
};