var LocalStrategy = require('passport-local').Strategy;

var User = require('../app/models/user');

module.exports = function(passport) {
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});

	passport.use('local-signup', new LocalStrategy({
		usernameField: 'visa',
		passwordField: 'password',
		passReqToCallback: true
	}, function(req, visa, password, done) {
		User.findOne({"visa": visa}, function(err, user) {
			if (err) {
				return done(err);
			}
			if (user) {
				return done(null, false, req.flash('signupMessage', 'That visa is already taken.'));
			} else {
				var newUser = new User();
				newUser.visa = visa;
				newUser.password = newUser.generateHash(password);
				newUser.save(function(err) {
					if (err) {
						throw err;
					} else {
						return done(null, newUser);
					}
				});
			}
		});
	}));

	passport.use('local-login', new LocalStrategy({
		usernameField: 'visa',
		passwordField: 'password',
		passReqToCallback: true
	}, function(req, visa, password, done) {
		User.findOne({'visa': visa}, function(err, user) {
			if (err) {
				return done(err);
			} else if (!user) {
				return done(null, user, req.flash('loginMessage', 'No user found.'));
			} else if (!user.validPassword(password)) {
				return done(null, user, req.flash('loginMessage', 'Wrong password.'));
			} else {
				return done(null, user);
			}
		});
	}));
};
