Installations:
1/ NodeJS
2/ MongoDB (Create folder at: C:\data\db - the mongodb data default folder saved)

Prepare running:
1/ Run cmd and cd to the project directory.
2/ Run: npm install
3/ Run: npm install -g nodemon
4/ Run mongodb first: Open another cmd, cd to the mongodb setup dir (C:\Program Files\MongoDB\Server\3.2\bin), run: mongod.exe
5/ Run: nodemon

Environment hackathon:
1/ Windows 7 or higher
2/ MongoDB
3/ NodeJS