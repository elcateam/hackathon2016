var express = require('express');
var router = express.Router();

var nodemailer = require('nodemailer');
var EmailTemplate = require('email-templates').EmailTemplate;

var mongoose = require('mongoose');
var Event = require('../app/models/event');

var smtpConfig = {
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // use SSL
    auth: {
        user: 'xuinservices@gmail.com',
        pass: '11947311'
    }
};

// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport(smtpConfig);

// create template based sender function
// assumes text.{ext} and html.{ext} in templates
var send = transporter.templateSender(new EmailTemplate('templates'), {
    from: '"EMT Services 👥" <xuinservices@gmail.com>',
});

/*send(
  {
    to: ['ppn@elca.vn'],
    // EmailTemplate renders html and text but no subject so we need to
    // set it manually either here or in the defaults section of templateSender()
    subject: '[EM ✔ T] Beer Party Club',
    attachments: [{
        filename: 'party.jpg',
        path: 'party.jpg',
        cid: 'event@img.cid' //same cid value as in the html img src
    }]
  },
  {
    // data using ejs expression
    title: 'Beer Party Club',
    content: `On 20/Oct/2016 we will organize a beer party at Clauderon Restaurant,
      133 Nguyen Thi Minh Khai, District 1. This will be large event and have a lot of VIP person.
      If you have spare time, we are welcome you and your family to join with us.
    `,
    name: 'Phong Nguyen'
  },
  function(err, info) {
    if(err){
       console.log(err);
    } else {
       console.log('Message sent: ' + info.response);
    }
  }
);
*/
router.get('/:emails/:id', function(req, res) {
  // use template based sender to send a message
  var emails = ['vinh.nsv@gmail.com'];
  var query = {"_id": req.params.id};
  var title;
  var content;
  Event.findOne(query).populate('_user', 'visa').exec(function(err, event) {
    console.log(event);
    title = event.name;
    content = event.content;
  });
  if (emails) {
    emails = req.params.emails.split(",");
  }
  console.log(emails);
  send(
    {
      to: emails,
      // EmailTemplate renders html and text but no subject so we need to
      // set it manually either here or in the defaults section of templateSender()
      subject: '[EM ✔ T] '+ title,
      attachments: [{
          filename: 'party.jpg',
          path: 'party.jpg',
          cid: 'event@img.cid' //same cid value as in the html img src
      }]
    },
    {
      // data using ejs expression
      title: title,
      content: content,
      name: 'EM ✔ T'
    },
    function(err, info) {
      if(err){
         console.log(err);
      } else {
         console.log('Message sent: ' + info.response);
      }
    }
  );

  res.redirect('/events/event/'+req.params.id);
});

module.exports = router;
