var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Event = require('../app/models/event');
var Comment = require('../app/models/comment');

router.get('/', function(req, res) {
  var currentDate = new Date();
  var monthInput = currentDate.getMonth();
  var yearInput = currentDate.getFullYear();

  var fromDate = new Date();
  fromDate.setDate(1);
  fromDate.setFullYear(yearInput);
  fromDate.setMonth(monthInput);
  var toDate = new Date();
  toDate.setFullYear(fromDate.getFullYear());
  toDate.setMonth(fromDate.getMonth() + 1);
  toDate.setDate(1);

  var nextMonth, nextYear, previousMonth, previousYear;

  if (monthInput == 11) {
    nextMonth = 0;
    nextYear = yearInput + 1;
    previousMonth = monthInput - 1;
    previousYear = yearInput;
  } else if (monthInput == 0) {
    previousMonth = 11;
    previousYear = yearInput - 1;
    nextMonth = monthInput + 1;
    nextYear = yearInput;
  } else {
    nextYear = yearInput;
    previousYear = yearInput;
    previousMonth = monthInput - 1;
    nextMonth = monthInput + 1;
  }
  var query = {'start_date': {'$gte': fromDate, '$lt': toDate}};
  if (req.query.quickSearchText != '') {
    query = {'start_date': {'$gte': fromDate, '$lt': toDate}, name: new RegExp(req.query.quickSearchText, "i")};
  }
  Event.find(query).sort({start_date: 'ascending'}).populate('_user', 'visa').exec(function(err, events) {
    console.log(events);
    res.render('events', { title: 'List events', events: events, quickSearchText: req.query.quickSearchText, year: yearInput, month: monthInput, previousMonth: previousMonth, previousYear: previousYear, nextMonth: nextMonth, nextYear: nextYear});
  });
});

router.get('/searchFrom/:year/:month', function(req, res) {
  var currentDate = new Date();
  var monthInput, yearInput;
  if (req.params.month != '') {
    monthInput = Number(req.params.month);
  } else {
    monthInput = currentDate.getMonth();
  }
  if (req.params.year != '') {
    yearInput = Number(req.params.year);
  } else {
    yearInput = currentDate.getFullYear();
  }
  var fromDate = new Date();
  fromDate.setDate(1);
  fromDate.setFullYear(yearInput);
  fromDate.setMonth(monthInput);
  var toDate = new Date();
  toDate.setFullYear(fromDate.getFullYear());
  toDate.setMonth(fromDate.getMonth() + 1);
  toDate.setDate(1);

  var nextMonth, nextYear, previousMonth, previousYear;

  if (monthInput == 11) {
    nextMonth = 0;
    nextYear = yearInput + 1;
    previousMonth = monthInput - 1;
    previousYear = yearInput;
  } else if (monthInput == 0) {
    previousMonth = 11;
    previousYear = yearInput - 1;
    nextMonth = monthInput + 1;
    nextYear = yearInput;
  } else {
    nextYear = yearInput;
    previousYear = yearInput;
    previousMonth = monthInput - 1;
    nextMonth = monthInput + 1;
  }
  
  var query = {'start_date': {'$gte': fromDate, '$lt': toDate}};
  Event.find(query).sort({start_date: 'ascending'}).populate('_user', 'visa').exec(function(err, events) {
    console.log(events);
    res.render('events', { title: 'List events', events: events, year: yearInput, month: monthInput, previousMonth: previousMonth, previousYear: previousYear, nextMonth: nextMonth, nextYear: nextYear});
  });
});

router.get('/event/:id', function(req, res) {
  var query = {"_id": req.params.id};
  Event.findOne(query).populate('_user', 'visa').exec(function(err, event) {
    console.log(event);
    Comment.find({_event: req.params.id}).sort({created_at: 'descending'}).populate('_user', 'visa').exec(function(err, comments) {
      console.log(comments);
      res.render('event', {title : 'Event name: ' + event.name, event : event, comments: comments, userid: req.user.id });
    });
  });
});

router.get('/quickSearch/:text', function(req, res) {
  var query = {name: new RegExp(req.params.text, "i")};
  Event.find(query).sort({start_date: 'ascending'}).populate('_user', 'visa').exec(function(err, events) {
    res.render('events', { title: 'List events', events: events, quickSearchText: req.params.text});
  });
});

router.post('/', function(req, res) {
  new Event({name : req.body.name, _user: req.user.id, start_date: new Date(), end_date: new Date()})
  .save(function(err, event) {
    console.log(event);
    res.redirect('/events/event/' + event.id);
  });
});

router.put('/event/:id', function(req, res) {
  var query = {"_id": req.params.id};
  var startD = new Date(Date.parse(req.body.startDateTime));
  var endD = startD;
  if (req.body.endDateTime != '') {
    endD = new Date(Date.parse(req.body.endDateTime));
  }
  
  var update = {name : req.body.name, address : req.body.address, content: req.body.content, lat: req.body.latInput, lng: req.body.lngInput,
    start_date: startD, end_date: endD };
  var options = {new: true};

  Event.findOneAndUpdate(query, update, options).populate('_user', 'visa').exec(function(err, event) {
    console.log(event);
    Comment.find({_event: req.params.id}).sort({created_at: 'descending'}).populate('_user', 'visa').exec(function(err, comments) {
      console.log(comments);
      //res.render('event', {title : 'Event name: ' + event.name, event : event, comments: comments, userid: req.user.id });
      // TODO: redirect only in successful case
      res.redirect('/events');
    });
  });
});

router.delete('/event/:id', function(req, res) {
  var query = {"_id": req.params.id};
  Event.findOneAndRemove(query, function(err, event){
    console.log(event)
    res.redirect('/events');
  });
});

router.post('/event/:id', function(req, res) {
  new Comment({ _user: req.user.id, _event: req.params.id, content: req.body.commentContent })
  .save(function(err, comment) {
    console.log(comment);
  });
});

router.get('/dummyData', function(req, res) {
  for (var i = 0; i <= 10; i++) {
    new Event({name : 'Event' + i, _user: req.user.id}).save(function(err, event) {
      console.log(event);
      for (var j = 0; j <= 10; j++) {
        new Comment({ _user: req.user.id, _event: event.id, content: 'Comment' + j })
        .save(function(err, comment) {
          console.log(comment);
        });
      }
    });
  };
  res.redirect('/events');
});

module.exports = router;
