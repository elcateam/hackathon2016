var mongoose = require('mongoose');

var eventUserSchema = mongoose.Schema({
	_event: {type: mongoose.Schema.ObjectId, ref 'Event'},
	_user: {type: mongoose.Schema.ObjectId, ref 'User'}
});

module.exports = mongoose.model('EventUser', eventUserSchema);