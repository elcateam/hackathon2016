var mongoose = require('mongoose');

var eventSchema = mongoose.Schema({
	name : String,
	start_date : Date,
	end_date : Date,
	address : String,
	content: String,
	lat: String,
	lng: String,
	countComment: Number,
	_user: { type: mongoose.Schema.ObjectId, ref: 'User' },
	_category: {type: mongoose.Schema.ObjectId, ref: 'Category'},
	_comments : [{ type: mongoose.Schema.ObjectId, ref: 'Comment'}]
});

module.exports = mongoose.model('Event', eventSchema);
