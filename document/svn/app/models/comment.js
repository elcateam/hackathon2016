var mongoose = require('mongoose');

var commentSchema = mongoose.Schema({
	_user: { type: mongoose.Schema.ObjectId, ref: 'User' },
	_event: { type: mongoose.Schema.ObjectId, ref: 'Event' },
	content: String,
	created_at: Date,
  	updated_at: Date
});

module.exports = mongoose.model('Comment', commentSchema);
