angular.module('todoController', [])

	// inject the Todo service factory into our controller
	.controller('mainController', function($scope, $http, $mdDialog, Todos) {
		$scope.formData = {};

		// GET =====================================================================
		// when landing on the page, get all todos and show them
		// use the service to get all the todos
		Todos.get()
			.success(function(data) {
				$scope.todos = data;
			});

		$scope.getTodo = function(ev, id) {
			Todos.getTodo(id)
				// if successful creation, call our get function to get all the new todos
				.success(function(data) {
					$mdDialog.show({
						locals: {
						           todo: data
						         },
				      controller: DialogController,
				      templateUrl: 'todoDetail.html',
				      parent: angular.element(document.body),
				      targetEvent: ev,
				      fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
				    });
				});
			
		};

		// CREATE ==================================================================
		// when submitting the add form, send the text to the node API
		$scope.createTodo = function() {

			// validate the formData to make sure that something is there
			// if form is empty, nothing will happen
			if ($scope.formData.text != undefined) {

				// call the create function from our service (returns a promise object)
				Todos.create($scope.formData)

					// if successful creation, call our get function to get all the new todos
					.success(function(data) {
						$scope.formData = {}; // clear the form so our user is ready to enter another
						$scope.todos = data; // assign our new list of todos
					});
			}
		};

		// DELETE ==================================================================
		// delete a todo after checking it
		$scope.deleteTodo = function(id) {
			Todos.delete(id)
				// if successful creation, call our get function to get all the new todos
				.success(function(data) {
					$scope.todos = data; // assign our new list of todos
				});
		};

		function DialogController($scope, $mdDialog, todo) {
			$scope.todo = todo;
			$scope.todo.submissionDate = new Date(todo.submissionDate);
		    $scope.hide = function() {
		      $mdDialog.hide();
		    };

		    $scope.cancel = function() {
		      $mdDialog.cancel();
		    };

		    $scope.answer = function(answer) {
		      $mdDialog.hide(answer);
		    };
		  };
	});