CREATE TABLE public.comments
(
    id integer NOT NULL DEFAULT nextval('comments_id_seq'::regclass),
    user_id integer,
    event_id integer,
    content character varying COLLATE "default".pg_catalog,
    CONSTRAINT comments_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.comments
    OWNER to postgres;

CREATE TABLE public.events
(
    id integer NOT NULL DEFAULT nextval('events_id_seq'::regclass),
    user_id integer,
    category_id integer,
    name character varying COLLATE "default".pg_catalog,
    address character varying COLLATE "default".pg_catalog,
    content character varying COLLATE "default".pg_catalog,
    lat_position double precision,
    long_position double precision,
    CONSTRAINT events_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.events
    OWNER to postgres;

CREATE TABLE public.eventusers
(
    id integer NOT NULL DEFAULT nextval('eventusers_id_seq'::regclass),
    user_id integer,
    event_id integer,
    CONSTRAINT eventusers_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.eventusers
    OWNER to postgres;

CREATE TABLE public.users
(
    id integer NOT NULL DEFAULT nextval('users_id_seq'::regclass),
    visa character varying COLLATE "default".pg_catalog,
    password character varying COLLATE "default".pg_catalog,
    email text COLLATE "default".pg_catalog,
    CONSTRAINT users_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres;

DROP DATABASE IF EXISTS puppies;
CREATE DATABASE puppies;

\c puppies;

CREATE TABLE pups (
  ID SERIAL PRIMARY KEY,
  name VARCHAR,
  breed VARCHAR,
  age INTEGER,
  sex VARCHAR
);

INSERT INTO pups (name, breed, age, sex)
  VALUES ('Tyler', 'Shih-tzu', 3, 'M');