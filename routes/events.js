var express = require('express');
var router = express.Router();

var db = require('../config/database');

router.get('/', function(req, res) {
  db.any('select e.*, coalesce(c.commentCount, 0) as commentCount, coalesce(u.userCount, 0) as userCount from events as e left join (select event_id, count(id) as commentCount from comments group by event_id) as c on e.id = c.event_id left join (select event_id, count(id) as userCount from eventUsers group by event_id) as u on e.id = c.event_id')
    .then(function (events) {
      console.log(events);
      res.render('events', { title: 'List events', events: events});
    });
});

router.get('/myEvents', function(req, res) {
  db.any('select * from events where user_id = $1', req.user.id)
    .then(function (events) {
      console.log(events);
      res.render('events', { title: 'List events', events: events});
    });
});

router.get('/top9Comments', function(req, res) {
  
});

router.get('/event/:id', function(req, res) {
  db.one('select e.*, u.visa from (select * from events where id = $1) as e left join users as u on e.user_id = u.id', req.params.id)
  .then(function (event) {
    var cmts;
    db.any('select c.*, u.visa from (select * from comments where event_id = $1) as c left join users as u on c.user_id = u.id', req.params.id).then(function(comments) {
      cmts = comments;
    });
    db.any('select u.visa from (select * from eventUsers where event_id = 2) as e left join users as u on e.user_id = u.id', req.params.id).then(function(visas) {
      console.log(visas);
      res.render('event', {title : 'Event name: ' + event.name, event : event, comments: cmts, userid: req.user.id, visa: req.user.visa, visas: visas });
    })
  });
});

router.post('/', function(req, res) {
  db.result('insert into events (user_id, name) values($1, $2)', [req.user.id, req.body.name]).then(function (result) {
    console.log(result);
    res.redirect('/events');
  })
});

router.put('/event/:id', function(req, res) {
  db.none('update events set name=$1, content=$2, address=$3 where id=$4',
    [req.body.name, req.body.content, req.body.address, req.params.id])
    .then(function () {
      res.redirect('/events/event/' + req.params.id);
    });
});

router.delete('/event/:id', function(req, res) {
  db.result('delete from events where id = $1', req.params.id).then(function (result) {
    res.redirect('/events');
  });
});

module.exports = router;