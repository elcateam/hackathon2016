module.exports = function(app, passport) {
	app.get('/', isLoggedIn, function(req, res) {
		res.render('index');
	});

	app.get('/login', isLoggedIn, function(req, res) {
		res.render('login', { message: req.flash('loginMessage')});
	});

	app.post('/login', isLoggedIn, passport.authenticate('local-login', {
		successRedirect: '/events',
		failureRedirect: '/login',
		failureFlash: true
	}));

	app.get('/signup', isLoggedIn, function(req, res) {
		res.render('signup', { message: req.flash('signupMesage') });
	});

	app.post('/signup', isLoggedIn, passport.authenticate('local-signup', {
		successRedirect: '/events',
		failureRedirect: '/signup',
		failureFlash: true
	}));

	app.all('*', function(req, res, next) {
		if (req.isAuthenticated()) {
			return next();
		} else {
			res.redirect('/');
		}
	});

	app.get('/profile', function(req, res) {
		res.render('profile', { user: req.user });
	});

	app.get('/logout', function(req, res) {
		req.logout();
		res.redirect('/');
	});

	app.use('/events', require('./events'));
}

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) {
		res.redirect('/events');
	} else {
		return next();
	}
}