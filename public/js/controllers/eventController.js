angular.module('eventController', [])

	// inject the Todo service factory into our controller
	.controller('mainController', function($scope, $http, $mdDialog, Events) {
		$scope.formData = {};

		// GET =====================================================================
		$scope.getAllEvents = function(ev) {
			Events.getAllEvents().success(function(date) {
				$scope.events = data;
			});
		};

		$scope.getEvent = function(ev, id) {
			Events.getEvent(id).success(function(data) {
				$mdDialog.show({
					locals: {
						event: data
					},
					controller: DialogController,
					templateUrl: './eventDetail2.html',
					parent: angular.element(document.body),
					targetEvent: ev,
					fullscreen: $scope.customFullscreen
			    });
			});
		};

		// CREATE ==================================================================
		$scope.createEvent = function() {
			if ($scope.formData.text != undefined) {
				Events.createEvent($scope.formData).success(function(data) {
					$scope.formData = {};
					$scope.events = data;
				});
			}
		};

		// DELETE ==================================================================
		$scope.deleteEvent = function(id) {
			Events.deleteEvent(id).success(function(data) {
				$scope.events = data;
			});
		};

		function DialogController($scope, $mdDialog, event) {
			$scope.event = event;$scope.render = true;
			// $scope.todo.submissionDate = new Date(todo.submissionDate);
			$scope.hide = function() {
				$mdDialog.hide();
			};

			$scope.cancel = function() {
				$mdDialog.cancel();
			};

			$scope.answer = function(answer) {
				$mdDialog.hide(answer);
			};
		};
	});