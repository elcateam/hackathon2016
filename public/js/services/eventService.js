angular.module('eventService', [])
	.factory('Events', function($http) {
		return {
			getAllEvents : function() {
				return $http.get('/events');
			},
			getEvent : function(id) {
				return $http.get('/events/event/' + id);
			},
			createEvent : function(todoData) {
				return $http.post('/events/event', todoData);
			},
			deleteEvent : function(id) {
				return $http.delete('/events/event/' + id);
			}
		}
	});